Generating images from sketches

Zdroje informací:

- https://www.researchgate.net/profile/Den-Madsen/publication/353972591_Pix2Pix_GAN_for_Image-to-Image_Translation/links/611cd2c30c2bfa282a510d3b/Pix2Pix-GAN-for-Image-to-Image-Translation.pdf -https://arxiv.org/abs/2404.19265
- https://www.youtube.com/watch?v=9SGs4Nm0VR4&t=65s

Zadání:

- Použití pix2pix modelu, na generování obrázků ze skic
- Záhy jsem zjistil, že natrénovat takový model, by bylo mnohem složitější a výpočetně náročnější, než jsem si původně myslel. Tak se budu konkretizovat na generování obličejů ze skic.

Postup práce:

- Původně jsem zkoušel natrénoval na sadě obsahující 20 různých předmětů. ale po 20 hodinách tréninku, model pořád generoval obrázky, které nedávaly smysl.
- Zkombinoval jsem několik datasetů, tvořící shruba 2100 obrázků. Použil jsem předtrénovaný model pix2pix turbo - https://github.com/GaParmar/img2img-turbo

![alt text](images/1147_2.png)
![alt text](images/1147.png)

Ještě spustím trénování modelu na delší dobu a zkusím optimalizovat trénink.

Repozitář: https://gitlab.fit.cvut.cz/turekmat/mvi-sp
