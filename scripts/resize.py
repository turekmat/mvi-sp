import os
from PIL import Image, UnidentifiedImageError

# Path to your Datasets folder
dataset_path = './Datasets'

# Desired size for resizing (256x256)
resize_size = (256, 256)

# List to keep track of files that couldn't be resized
unprocessed_files = []

# Loop through all subdirectories and files
for root, dirs, files in os.walk(dataset_path):
    for file in files:
        file_path = os.path.join(root, file)
        # Check if the file is a .jpg or .png
        if file.lower().endswith(('.jpg', '.png')):
            try:
                # Open image with error handling for unreadable files
                with Image.open(file_path) as img:
                    # Convert RGBA to RGB if necessary
                    if img.mode == 'RGBA':
                        img = img.convert('RGB')
                    # Resize image and save it back to the same path
                    img = img.resize(resize_size, Image.Resampling.LANCZOS)
                    img.save(file_path)
                    print(f"Resized: {file_path}")
            except UnidentifiedImageError:
                print(f"Error: Cannot identify image file {file_path}. Skipping...")
                unprocessed_files.append(file_path)
            except Exception as e:
                print(f"Error processing {file_path}: {e}")
                unprocessed_files.append(file_path)
        else:
            # Remove non .jpg and .png files
            os.remove(file_path)
            print(f"Deleted: {file_path}")

# Print the names of files that couldn't be processed
if unprocessed_files:
    print("\nFiles that could not be resized:")
    for file in unprocessed_files:
        print(file)
else:
    print("\nAll files were processed successfully.")
