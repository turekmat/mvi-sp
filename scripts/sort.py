import json
import os

# Replace these paths with the actual paths on your machine
train_dir_A = './faces_dataset/train_A'
test_dir_A = './faces_dataset/test_A'

# Initialize dictionaries for prompts with "default prompt" as placeholder text
train_prompts = {}
test_prompts = {}

# Populate train prompts
for filename in sorted(os.listdir(train_dir_A)):
    if filename.endswith('.png'):  # Only process .png files
        train_prompts[filename] = "default prompt"  # Placeholder prompt

# Populate test prompts
for filename in sorted(os.listdir(test_dir_A)):
    if filename.endswith('.png'):
        test_prompts[filename] = "default prompt"  # Placeholder prompt

# Save train_prompts.json
with open('train_prompts.json', 'w') as f:
    json.dump(train_prompts, f, indent=4)

# Save test_prompts.json
with open('test_prompts.json', 'w') as f:
    json.dump(test_prompts, f, indent=4)

print("Prompt files with 'default prompt' generated successfully.")
