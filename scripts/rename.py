import os
from PIL import Image

# Directories for the source and target images
source_dir = '/Users/matyasturek/skola/mvi-sp/FacesSketches/faces_dataset/A/train'
target_dir = '/Users/matyasturek/skola/mvi-sp/FacesSketches/faces_dataset/B/train'

# Create sorted lists of all images in the source and target directories
source_images = sorted([f for f in os.listdir(source_dir) if f.endswith(('.png', '.jpg'))])
target_images = sorted([f for f in os.listdir(target_dir) if f.endswith(('.png', '.jpg'))])

# Check if the directories have the same number of images
assert len(source_images) == len(target_images), "Source and target directories do not contain the same number of images."

# Loop through each pair and convert, rename, and save in order
for i, (source_img, target_img) in enumerate(zip(source_images, target_images), start=1):
    # Generate new filenames
    new_filename = f"{i}.png"

    # Convert and save source image as .png with new filename
    source_path = os.path.join(source_dir, source_img)
    img = Image.open(source_path)
    img.convert('RGB').save(os.path.join(source_dir, new_filename), 'PNG')

    # Convert and save target image as .png with the same new filename
    target_path = os.path.join(target_dir, target_img)
    img = Image.open(target_path)
    img.convert('RGB').save(os.path.join(target_dir, new_filename), 'PNG')

    # Remove original files if they were .jpg
    if source_img.endswith('.jpg'):
        os.remove(source_path)
    if target_img.endswith('.jpg'):
        os.remove(target_path)

print("Conversion and renaming completed successfully.")
