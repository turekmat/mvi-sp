import os

# Base directory paths
base_path = './Datasets'
dir_A = os.path.join(base_path, 'A')
dir_B = os.path.join(base_path, 'B')

# List of files to delete from A and their corresponding files in B
files_to_delete = [
    'image_1559.jpg',
    'image_1554.jpg',
    'image_1569.jpg',
    'image_1543.jpg',
    'image_1580.jpg',
    'image_1592.jpg',
    'image_1587.jpg',
    'image_1561.jpg'
]

# Directories to search for the files in A and B
dirs_A = ['train', 'val']
dirs_B = ['train', 'val']

for file_name in files_to_delete:
    for subdir in dirs_A:
        file_path_A = os.path.join(dir_A, subdir, file_name)
        if os.path.exists(file_path_A):
            os.remove(file_path_A)
            print(f"Deleted {file_path_A} from A")
    
    for subdir in dirs_B:
        file_path_B = os.path.join(dir_B, subdir, file_name)
        if os.path.exists(file_path_B):
            os.remove(file_path_B)
            print(f"Deleted {file_path_B} from B")

print("Selected files and their corresponding pairs have been deleted.")

