
# Generování obličejů ze skic

## Popis
Projekt se zaměřuje na generování realistických obličejů z ručně kreslených skic pomocí modelu **Pix2Pix-Turbo**. Model využívá pokročilé optimalizace pro převod jednoho typu obrazu na druhý, což umožňuje efektivní a realistickou rekonstrukci obličejů.

## Data
Pro trénink byl použit dataset složený z:
- **[CUHK Face Sketch Database (CUFS)](https://www.ee.cuhk.edu.hk/~xgwang/CUFS.html)**
- **[FS2K](https://github.com/DengPingFan/FS2K?tab=readme-ov-file)**

Celkově obsahoval **2234 párů** skic a fotografií, které byly před tréninkem normalizovány na rozměr 256x256 px.

## Metody
Model **[Pix2Pix-Turbo](https://github.com/GaParmar/img2img-turbo)** byl trénován na **Google Colab** s GPU, kde proběhlo **15 000 kroků**. Byly zkoušeny další optimalizace jako residual bloky a style loss, ale nejlepších výsledků bylo dosaženo s původní architekturou.

## Výsledky
Model dosáhl realistických výsledků při generování obličejů. Ukázalo se, že vyšší rozlišení dat by mohlo kvalitu dále zlepšit, což však nebylo realizováno kvůli omezeným výpočetním zdrojům.

## Závěr
Tento projekt demonstruje schopnosti modelu Pix2Pix-Turbo při generování obličejů ze skic. Pro další zlepšení doporučuji zvýšit rozlišení dat a případně optimalizovat ztrátové funkce.

